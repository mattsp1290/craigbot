require 'isaac'
require './weather_responder'
require './twitch_responder'

configure do |c|
  c.nick     = "Craig_Bot"
  c.server   = "irc.twitch.tv"
  c.port     = 6667
  c.password = ""
  c.verbose  = true
end

helpers do
  @user = "punk1290"
  @chan = '#punk1290'
  @commands = {
    "!help" => {
      :type => :help,
      :description => "Gives a list of available commands and describes them."
    },
    "!uptime" => {
      :type => :method_call,
      :source => TwitchResponder,
      :description => "Amount of time the stream has been live."
    },
    "!social" => {
      :type => :message,
      :content => "You can follow me on twitter at http://twitter.com/punk1290",
      :description => "Places where you can follow or contact #{@user}"
    },
    "!weather" => {
      :type => :method_call,
      :source => WeatherResponder,
      :description => "A description of the weather local to #{@user}. If instead you type !weater forecast a forecast for the rest of the day will be presented in common language."
    }
  }
end
