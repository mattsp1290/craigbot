require 'twitch'

class TwitchResponder
  attr_accessor :twitch

  def self.respond(event)
    @twitch = Twitch.new()
    command = event.message.split(" ")[0][1..-1]
    message = ""
    case command
      when "uptime"
        message = self.uptime(event.channel[1..-1])
    end
    return message
  end

  def self.uptime(channel)
    stream = @twitch.stream(channel)
    if stream
      time = Time.parse(stream[:body]["stream"]["created_at"])
      puts time
      puts Time.now
      elapsed_time = (Time.now.to_i - time.to_i) / 60
      return "#{elapsed_time} minutes"
    end
  end
end
