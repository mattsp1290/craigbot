require 'weather-underground'
class WeatherResponder
  def self.respond(event)
  	weather = WeatherUnderground::Base.new
    if event.message.downcase.include? "weather"
    	message = ""
  	  if event.message.downcase.include? "forecast"
  			weather.TextForecast(50309).days.each { |day| message += "#{day.title}: #{day.text} \n\n"}
  	  else
  	  	today = weather.CurrentObservations(50309)
  	  	message = "It is currently #{today.temperature_string} and #{today.weather.downcase}"
  	  end
  	  return message
  	else
  	  return ''
  	end
  end
end
