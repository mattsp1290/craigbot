class HelpResponder
  def self.respond(commands)
    message = []
    commands.keys.each do |key|
      command = commands[key]
      message += ["#{key}:"]
      message += [command[:description]]
    end
    return message
  end
end
