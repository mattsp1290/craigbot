require 'isaac'
require 'uri'
require './weather_responder'
require './help_responder'
require './twitch_responder'
require './config'
require 'twitch'



on :connect do
  join @chan
end

on :channel do
  if message.chars.first == "!"
      words = message.downcase.split(" ")
      potential_command = words[0]
      if @commands.has_key? potential_command
        command = @commands[potential_command]
        case command[:type]
          when :method_call
            twitch_message = TwitchMessage.new(channel, message, nick)
            msg channel, command[:source].respond(twitch_message)
          when :message
            msg channel, command[:content]
          when :help
            messages = HelpResponder.respond(@commands)
            messages.each do |message|
              msg channel, message
            end
        end
      else
        msg channel, "#{potential_command} is an unkown command. You can type !help for more information about available commands."
      end
  end
end

class TwitchMessage
  attr_accessor :channel, :message, :nick
  def initialize( channel, message, nick )
    @channel = channel
    @message = message
    @nick = nick
  end
end
